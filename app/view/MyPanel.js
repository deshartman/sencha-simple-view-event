/*
 * File: app/view/MyPanel.js
 *
 * This file was generated by Sencha Architect version 2.0.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.0.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.MyPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.myPanel',

    config: {
        padding: 50,
        items: [
            {
                xtype: 'button',
                itemId: 'mybutton',
                maxHeight: 37,
                maxWidth: 400,
                text: 'Press Me',
                action: 'SEND_MESSAGE'
            },
            {
                xtype: 'button',
                itemId: 'btnShout',
                maxWidth: 400,
                text: 'SHOUT',
                action: 'SHOUT'
            },
            {
                xtype: 'fieldset',
                defaults: {
                    action: 'TEXT_CHANGE'
                },
                title: 'MyFieldSet',
                items: [
                    {
                        xtype: 'textfield',
                        label: 'Field'
                    },
                    {
                        xtype: 'textfield',
                        label: 'Field'
                    }
                ]
            }
        ]
    }

});